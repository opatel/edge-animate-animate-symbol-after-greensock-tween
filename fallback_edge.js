/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};
var opts = {};
var resources = [
];
var symbols = {
"stage": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "both",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
            {
                id: 'icons2',
                type: 'rect',
                rect: ['99', '60','auto','auto','auto', 'auto']
            },
            {
                id: 'stage',
                type: 'image',
                rect: ['162px', '88px','224px','224px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"stage.png",'0px','0px']
            }],
            symbolInstances: [
            {
                id: 'icons2',
                symbolName: 'icons',
                autoPlay: {

                }
            }
            ]
        },
    states: {
        "Base State": {
            "${_Stage}": [
                ["color", "background-color", 'rgba(255,255,255,1)'],
                ["style", "overflow", 'hidden'],
                ["style", "height", '400px'],
                ["style", "width", '550px']
            ],
            "${_icons2}": [
                ["style", "top", '60px'],
                ["subproperty", "filter.sepia", '0.02'],
                ["style", "left", '99px'],
                ["style", "-webkit-transform-origin", [49.99,42.04], {valueTemplate:'@@0@@% @@1@@%'} ],
                ["style", "-moz-transform-origin", [49.99,42.04],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-ms-transform-origin", [49.99,42.04],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "msTransformOrigin", [49.99,42.04],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-o-transform-origin", [49.99,42.04],{valueTemplate:'@@0@@% @@1@@%'}]
            ],
            "${_stage}": [
                ["style", "top", '88px'],
                ["style", "height", '224px'],
                ["style", "left", '162px'],
                ["style", "width", '224px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
                { id: "eid152", tween: [ "subproperty", "${_icons2}", "filter.sepia", '0.02', { fromValue: '0.02'}], position: 0, duration: 0 }            ]
        }
    }
},
"discs": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'colordots',
                    type: 'group',
                    rect: ['0', '0', '267', '271', 'auto', 'auto'],
                    c: [
                    {
                        rect: ['123px', '0px', '21px', '21px', 'auto', 'auto'],
                        borderRadius: ['50%', '50%', '50%', '50%'],
                        id: 'red',
                        stroke: [0, 'rgb(0, 0, 0)', 'none'],
                        type: 'ellipse',
                        fill: ['rgba(249,0,0,1.00)']
                    },
                    {
                        rect: ['123px', '250px', '21px', '21px', 'auto', 'auto'],
                        borderRadius: ['50%', '50%', '50%', '50%'],
                        id: 'yellow',
                        stroke: [0, 'rgb(0, 0, 0)', 'none'],
                        type: 'ellipse',
                        fill: ['rgba(246,227,9,1.00)']
                    },
                    {
                        rect: ['246px', '126px', '21px', '21px', 'auto', 'auto'],
                        borderRadius: ['50%', '50%', '50%', '50%'],
                        id: 'green',
                        stroke: [0, 'rgb(0, 0, 0)', 'none'],
                        type: 'ellipse',
                        fill: ['rgba(67,255,0,1.00)']
                    },
                    {
                        rect: ['0px', '126px', '21px', '21px', 'auto', 'auto'],
                        borderRadius: ['50%', '50%', '50%', '50%'],
                        id: 'blue',
                        stroke: [0, 'rgb(0, 0, 0)', 'none'],
                        type: 'ellipse',
                        fill: ['rgba(7,25,246,1.00)']
                    }]
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_green}": [
                ["style", "top", '126px'],
                ["style", "left", '246px'],
                ["color", "background-color", 'rgba(67,255,0,1.00)']
            ],
            "${_colordots}": [
                ["transform", "rotateZ", '0deg']
            ],
            "${_red}": [
                ["style", "top", '0px'],
                ["style", "left", '123px'],
                ["color", "background-color", 'rgba(249,0,0,1.00)']
            ],
            "${_blue}": [
                ["style", "top", '126px'],
                ["style", "left", '0px'],
                ["color", "background-color", 'rgba(7,25,246,1.00)']
            ],
            "${_yellow}": [
                ["style", "top", '250px'],
                ["style", "left", '123px'],
                ["color", "background-color", 'rgba(246,227,9,1.00)']
            ],
            "${symbolSelector}": [
                ["style", "height", '271px'],
                ["style", "width", '267px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 1500,
            autoPlay: false,
            labels: {
                "redtop": 0,
                "bluetop": 500,
                "yelowtop": 1000,
                "greentop": 1500
            },
            timeline: [
            ]
        }
    }
},
"Hopital": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'Hopital',
                    type: 'image',
                    rect: ['0px', '0px', '75px', '67px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/Hopital.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_Hopital}": [
                ["style", "top", '0px'],
                ["style", "height", '67px'],
                ["style", "left", '0px'],
                ["style", "width", '75px']
            ],
            "${symbolSelector}": [
                ["style", "height", '67px'],
                ["style", "width", '75px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"beach": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'beach',
                    type: 'image',
                    rect: ['0px', '0px', '75px', '99px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/beach.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_beach}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '99px'],
                ["style", "width", '75px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"resto": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'resto',
                    type: 'image',
                    rect: ['0px', '0px', '48px', '79px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/resto.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_resto}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '79px'],
                ["style", "width", '48px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"seight": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'seight',
                    type: 'image',
                    rect: ['0px', '0px', '155px', '63px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/seight.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_seight}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '63px'],
                ["style", "width", '155px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"transport": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'transport',
                    type: 'image',
                    rect: ['0px', '0px', '105px', '66px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/transport.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_transport}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '66px'],
                ["style", "width", '105px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"act": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'act',
                    type: 'image',
                    rect: ['0px', '0px', '121px', '64px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/act.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_act}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '64px'],
                ["style", "width", '121px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"hotel": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'hotel',
                    type: 'image',
                    rect: ['0px', '0px', '121px', '102px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/hotel.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_hotel}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '102px'],
                ["style", "width", '121px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"aero": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'aero',
                    type: 'image',
                    rect: ['0px', '0px', '90px', '100px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/aero.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_aero}": [
                ["style", "top", '0px'],
                ["style", "left", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '100px'],
                ["style", "width", '90px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"icons": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'transport_sym',
                    type: 'rect',
                    rect: ['71', '34', 'auto', 'auto', 'auto', 'auto']
                },
                {
                    id: 'hopital_sym',
                    type: 'rect',
                    rect: ['160', '110', 'auto', 'auto', 'auto', 'auto']
                },
                {
                    id: 'Group',
                    type: 'group',
                    rect: ['16px', '10px', '335', '312', 'auto', 'auto'],
                    c: [
                    {
                        id: 'aero2',
                        type: 'rect',
                        rect: ['-56px', '57px', 'auto', 'auto', 'auto', 'auto']
                    },
                    {
                        id: 'Hopital2',
                        type: 'rect',
                        rect: ['217px', '6px', 'auto', 'auto', 'auto', 'auto']
                    },
                    {
                        id: 'hotel2',
                        type: 'rect',
                        rect: ['-116px', '-71px', 'auto', 'auto', 'auto', 'auto']
                    },
                    {
                        id: 'act2',
                        type: 'rect',
                        rect: ['-116px', '-71px', 'auto', 'auto', 'auto', 'auto']
                    },
                    {
                        id: 'transport2',
                        type: 'rect',
                        rect: ['-116px', '-71px', 'auto', 'auto', 'auto', 'auto']
                    },
                    {
                        id: 'seight2',
                        type: 'rect',
                        rect: ['-116px', '-71px', 'auto', 'auto', 'auto', 'auto']
                    },
                    {
                        id: 'resto2',
                        type: 'rect',
                        rect: ['-116px', '-71px', 'auto', 'auto', 'auto', 'auto']
                    },
                    {
                        id: 'beach2',
                        type: 'rect',
                        rect: ['-116px', '-71px', 'auto', 'auto', 'auto', 'auto']
                    }]
                }
            ],
            symbolInstances: [
            {
                id: 'Hopital2',
                symbolName: 'Hopital',
                autoPlay: {

               }
            },
            {
                id: 'seight2',
                symbolName: 'seight',
                autoPlay: {

               }
            },
            {
                id: 'resto2',
                symbolName: 'resto',
                autoPlay: {

               }
            },
            {
                id: 'aero2',
                symbolName: 'aero',
                autoPlay: {

               }
            },
            {
                id: 'hotel2',
                symbolName: 'hotel',
                autoPlay: {

               }
            },
            {
                id: 'transport_sym',
                symbolName: 'transport_sym',
                autoPlay: {

               }
            },
            {
                id: 'beach2',
                symbolName: 'beach',
                autoPlay: {

               }
            },
            {
                id: 'transport2',
                symbolName: 'transport',
                autoPlay: {

               }
            },
            {
                id: 'act2',
                symbolName: 'act',
                autoPlay: {

               }
            },
            {
                id: 'hopital_sym',
                symbolName: 'hopital_sym',
                autoPlay: {

               }
            }            ]
        },
    states: {
        "Base State": {
            "${_resto2}": [
                ["style", "top", '68px'],
                ["transform", "scaleY", '0.64631'],
                ["transform", "rotateZ", '72deg'],
                ["transform", "scaleX", '0.64631'],
                ["style", "left", '266px']
            ],
            "${_seight2}": [
                ["style", "top", '-23px'],
                ["transform", "scaleY", '0.58065'],
                ["style", "left", '80px'],
                ["transform", "scaleX", '0.58065']
            ],
            "${_Hopital2}": [
                ["style", "top", '96px'],
                ["style", "left", '-19px'],
                ["transform", "rotateZ", '-88deg']
            ],
            "${_act2}": [
                ["style", "top", '189px'],
                ["transform", "scaleY", '0.57158'],
                ["transform", "rotateZ", '-141deg'],
                ["transform", "scaleX", '0.57158'],
                ["style", "left", '14px']
            ],
            "${_Group}": [
                ["style", "top", '10px'],
                ["style", "left", '16px'],
                ["style", "-webkit-transform-origin", [47.76,41.34], {valueTemplate:'@@0@@% @@1@@%'} ],
                ["style", "-moz-transform-origin", [47.76,41.34],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-ms-transform-origin", [47.76,41.34],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "msTransformOrigin", [47.76,41.34],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-o-transform-origin", [47.76,41.34],{valueTemplate:'@@0@@% @@1@@%'}]
            ],
            "${_transport2}": [
                ["style", "top", '17px'],
                ["transform", "scaleY", '0.56835'],
                ["transform", "rotateZ", '-51deg'],
                ["transform", "scaleX", '0.56835'],
                ["style", "left", '6px']
            ],
            "${_beach2}": [
                ["style", "top", '149px'],
                ["transform", "scaleY", '0.51574'],
                ["transform", "rotateZ", '127deg'],
                ["transform", "scaleX", '0.51574'],
                ["style", "left", '236px']
            ],
            "${_hotel2}": [
                ["style", "top", '-12px'],
                ["transform", "scaleY", '0.50057'],
                ["transform", "rotateZ", '48deg'],
                ["transform", "scaleX", '0.50057'],
                ["style", "left", '195px']
            ],
            "${_hopital_sym}": [
                ["style", "left", '71px'],
                ["style", "top", '34px']
            ],
            "${_aero2}": [
                ["style", "top", '219px'],
                ["transform", "scaleY", '0.66308'],
                ["transform", "rotateZ", '180deg'],
                ["transform", "scaleX", '0.66308'],
                ["style", "left", '119px']
            ],
            "${symbolSelector}": [
                ["style", "height", '322px'],
                ["style", "width", '351px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
                { id: "eid69", tween: [ "transform", "${_transport2}", "rotateZ", '-51deg', { fromValue: '-51deg'}], position: 0, duration: 0 },
                { id: "eid65", tween: [ "transform", "${_transport2}", "scaleX", '0.56835', { fromValue: '0.56835'}], position: 0, duration: 0 },
                { id: "eid121", tween: [ "transform", "${_act2}", "scaleX", '0.57158', { fromValue: '0.57158'}], position: 0, duration: 0 },
                { id: "eid60", tween: [ "transform", "${_beach2}", "rotateZ", '127deg', { fromValue: '127deg'}], position: 0, duration: 0 },
                { id: "eid42", tween: [ "style", "${_aero2}", "left", '119px', { fromValue: '119px'}], position: 0, duration: 0 },
                { id: "eid130", tween: [ "style", "${_act2}", "top", '189px', { fromValue: '189px'}], position: 0, duration: 0 },
                { id: "eid132", tween: [ "transform", "${_act2}", "rotateZ", '-141deg', { fromValue: '-141deg'}], position: 0, duration: 0 },
                { id: "eid53", tween: [ "transform", "${_beach2}", "scaleX", '0.51574', { fromValue: '0.51574'}], position: 0, duration: 0 },
                { id: "eid90", tween: [ "style", "${_resto2}", "top", '68px', { fromValue: '68px'}], position: 0, duration: 0 },
                { id: "eid82", tween: [ "transform", "${_resto2}", "scaleX", '0.64631', { fromValue: '0.64631'}], position: 0, duration: 0 },
                { id: "eid83", tween: [ "transform", "${_resto2}", "scaleY", '0.64631', { fromValue: '0.64631'}], position: 0, duration: 0 },
                { id: "eid122", tween: [ "transform", "${_act2}", "scaleY", '0.57158', { fromValue: '0.57158'}], position: 0, duration: 0 },
                { id: "eid103", tween: [ "style", "${_hotel2}", "left", '195px', { fromValue: '195px'}], position: 0, duration: 0 },
                { id: "eid70", tween: [ "style", "${_transport2}", "left", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
                { id: "eid34", tween: [ "transform", "${_aero2}", "scaleX", '0.66308', { fromValue: '0.66308'}], position: 0, duration: 0 },
                { id: "eid24", tween: [ "style", "${_Hopital2}", "top", '96px', { fromValue: '96px'}], position: 0, duration: 0 },
                { id: "eid41", tween: [ "style", "${_aero2}", "top", '219px', { fromValue: '219px'}], position: 0, duration: 0 },
                { id: "eid89", tween: [ "style", "${_resto2}", "left", '266px', { fromValue: '266px'}], position: 0, duration: 0 },
                { id: "eid28", tween: [ "transform", "${_seight2}", "scaleY", '0.58065', { fromValue: '0.58065'}], position: 0, duration: 0 },
                { id: "eid104", tween: [ "style", "${_hotel2}", "top", '-12px', { fromValue: '-12px'}], position: 0, duration: 0 },
                { id: "eid71", tween: [ "style", "${_transport2}", "top", '17px', { fromValue: '17px'}], position: 0, duration: 0 },
                { id: "eid131", tween: [ "style", "${_Hopital2}", "left", '-19px', { fromValue: '-19px'}], position: 0, duration: 0 },
                { id: "eid43", tween: [ "transform", "${_Hopital2}", "rotateZ", '-88deg', { fromValue: '-88deg'}], position: 0, duration: 0 },
                { id: "eid88", tween: [ "transform", "${_resto2}", "rotateZ", '72deg', { fromValue: '72deg'}], position: 0, duration: 0 },
                { id: "eid61", tween: [ "style", "${_beach2}", "left", '236px', { fromValue: '236px'}], position: 0, duration: 0 },
                { id: "eid27", tween: [ "transform", "${_seight2}", "scaleX", '0.58065', { fromValue: '0.58065'}], position: 0, duration: 0 },
                { id: "eid66", tween: [ "transform", "${_transport2}", "scaleY", '0.56835', { fromValue: '0.56835'}], position: 0, duration: 0 },
                { id: "eid105", tween: [ "transform", "${_hotel2}", "rotateZ", '48deg', { fromValue: '48deg'}], position: 0, duration: 0 },
                { id: "eid44", tween: [ "transform", "${_aero2}", "rotateZ", '180deg', { fromValue: '180deg'}], position: 0, duration: 0 },
                { id: "eid100", tween: [ "transform", "${_hotel2}", "scaleY", '0.50057', { fromValue: '0.50057'}], position: 0, duration: 0 },
                { id: "eid33", tween: [ "style", "${_seight2}", "left", '80px', { fromValue: '80px'}], position: 0, duration: 0 },
                { id: "eid54", tween: [ "transform", "${_beach2}", "scaleY", '0.51574', { fromValue: '0.51574'}], position: 0, duration: 0 },
                { id: "eid35", tween: [ "transform", "${_aero2}", "scaleY", '0.66308', { fromValue: '0.66308'}], position: 0, duration: 0 },
                { id: "eid62", tween: [ "style", "${_beach2}", "top", '149px', { fromValue: '149px'}], position: 0, duration: 0 },
                { id: "eid129", tween: [ "style", "${_act2}", "left", '14px', { fromValue: '14px'}], position: 0, duration: 0 },
                { id: "eid99", tween: [ "transform", "${_hotel2}", "scaleX", '0.50057', { fromValue: '0.50057'}], position: 0, duration: 0 },
                { id: "eid32", tween: [ "style", "${_seight2}", "top", '-23px', { fromValue: '-23px'}], position: 0, duration: 0 }            ]
        }
    }
},
"transport_sym": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    rect: ['0px', '0px', '211px', '211px', 'auto', 'auto'],
                    borderRadius: ['50%', '50%', '50%', '50%'],
                    stroke: [0, 'rgba(0,0,0,1)', 'none'],
                    id: 'Ellipse',
                    opacity: 0,
                    type: 'ellipse',
                    fill: ['rgba(192,192,192,1)']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_Ellipse}": [
                ["style", "top", '0px'],
                ["style", "opacity", '0'],
                ["style", "left", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '211px'],
                ["style", "width", '211px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 2000,
            autoPlay: false,
            timeline: [
                { id: "eid239", tween: [ "style", "${_Ellipse}", "opacity", '0.98780487804878', { fromValue: '0'}], position: 0, duration: 1000 },
                { id: "eid240", tween: [ "style", "${_Ellipse}", "opacity", '0', { fromValue: '0.98780487804878'}], position: 1000, duration: 1000 }            ]
        }
    }
},
"hopital_sym": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    rect: ['0px', '0px', '211px', '211px', 'auto', 'auto'],
                    borderRadius: ['50%', '50%', '50%', '50%'],
                    id: 'Ellipse',
                    stroke: [0, 'rgba(0,0,0,1)', 'none'],
                    type: 'ellipse',
                    fill: ['rgba(192,192,192,1)']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_Ellipse}": [
                ["style", "top", '0px'],
                ["style", "opacity", '0'],
                ["style", "left", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '211px'],
                ["style", "width", '211px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 2000,
            autoPlay: false,
            timeline: [
                { id: "eid239", tween: [ "style", "${_Ellipse}", "opacity", '0.98780487804878', { fromValue: '0'}], position: 0, duration: 1000 },
                { id: "eid240", tween: [ "style", "${_Ellipse}", "opacity", '0', { fromValue: '0.98780487804878'}], position: 1000, duration: 1000 }            ]
        }
    }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources, opts);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-1374010289");
