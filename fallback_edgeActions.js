/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'discs'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_blue}", "mouseover", function(sym, e) {
         // Gets an element. For example, 
         // var element = sym.$("Text2");
         // element.hide();
         var bluedot = sym.$("colordots");
         TweenMax.to(bluedot, 2, {css:{rotation:90}});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_green}", "mouseover", function(sym, e) {
         var greendot = sym.$("colordots");
         TweenMax.to(greendot, 2, {css:{rotation:-90}});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_yellow}", "mouseover", function(sym, e) {
         var yellowdot = sym.$("colordots");
         TweenMax.to(yellowdot, 2, {css:{rotation:180}});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_red}", "mouseover", function(sym, e) {
         var reddot = sym.$("colordots");
         TweenMax.to(reddot, 2, {css:{rotation:0}});

      });
      //Edge binding end

   })("discs");
   //Edge symbol end:'discs'

   //=========================================================
   
   //Edge symbol: 'Hopital'
   (function(symbolName) {   
   
   })("Hopital");
   //Edge symbol end:'Hopital'

   //=========================================================
   
   //Edge symbol: 'beach'
   (function(symbolName) {   
   
   })("beach");
   //Edge symbol end:'beach'

   //=========================================================
   
   //Edge symbol: 'resto'
   (function(symbolName) {   
   
   })("resto");
   //Edge symbol end:'resto'

   //=========================================================
   
   //Edge symbol: 'seight'
   (function(symbolName) {   
   
   })("seight");
   //Edge symbol end:'seight'

   //=========================================================
   
   //Edge symbol: 'transport'
   (function(symbolName) {   
   
   })("transport");
   //Edge symbol end:'transport'

   //=========================================================
   
   //Edge symbol: 'act'
   (function(symbolName) {   
   
   })("act");
   //Edge symbol end:'act'

   //=========================================================
   
   //Edge symbol: 'hotel'
   (function(symbolName) {   
   
   })("hotel");
   //Edge symbol end:'hotel'

   //=========================================================
   
   //Edge symbol: 'aero'
   (function(symbolName) {   
   
   })("aero");
   //Edge symbol end:'aero'

   //=========================================================
   
   //Edge symbol: 'icons'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_seight2}", "mouseover", function(sym, e) {
         // insert code to be run when the mouse hovers over the object
         var seight2 = sym.$("Group");
         TweenMax.to(seight2, 2, {css:{rotation:0}});
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_aero2}", "mouseover", function(sym, e) {
         // insert code to be run when the mouse hovers over the object
         var aero2 = sym.$("Group");
         TweenMax.to(aero2, 2, {css:{rotation:180}});
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hotel2}", "mouseover", function(sym, e) {
         // insert code to be run when the mouse hovers over the object
         var hotel2 = sym.$("Group");
         TweenMax.to(hotel2, 2, {css:{rotation:-45}});
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_resto2}", "mouseover", function(sym, e) {
         // insert code to be run when the mouse hovers over the object
         var resto2 = sym.$("Group");
         TweenMax.to(resto2, 2, {css:{rotation:-45}});
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_beach2}", "mouseover", function(sym, e) {
         // insert code to be run when the mouse hovers over the object
         var beach2 = sym.$("Group");
         TweenMax.to(beach2, 2, {css:{rotation:-45}});
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_transport2}", "mouseover", function(sym, e) {
         // insert code to be run when the mouse hovers over the object
         var transport2 = sym.$("Group");
         TweenMax.to(transport2, 2, {css:{rotation:45},onComplete:function(){
             sym.getSymbol("transport_sym").play();
         }});
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_Hopital2}", "mouseover", function(sym, e) {
         // insert code to be run when the mouse hovers over the object
         var transport2 = sym.$("Group");
         TweenMax.to(transport2, 2, {css:{rotation:90},onComplete:function(){
             sym.getSymbol("hopital_sym").play();
         }});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_act2}", "mouseover", function(sym, e) {
         // insert code to be run when the mouse hovers over the object
         var act2 = sym.$("Group");
         TweenMax.to(act2, 2, {css:{rotation:45}});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_seight2}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("http://www.adobe.com", "_self");
         

      });
      //Edge binding end

   })("icons");
   //Edge symbol end:'icons'

   //=========================================================
   
   //Edge symbol: 'transport_sym'
   (function(symbolName) {   
   
   })("transport_sym");
   //Edge symbol end:'transport_sym'

   //=========================================================
   
   //Edge symbol: 'transport_sym_1'
   (function(symbolName) {   
   
   })("hopital_sym");
   //Edge symbol end:'hopital_sym'

})(jQuery, AdobeEdge, "EDGE-1374010289");